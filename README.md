Helps to execute JavaScript code step-by-step.

# ◦ Requirements

- Vanilla JS

# ◦ Install

### Via hosting server
Latest
```html
<script src="https://philippn.com/vendor/js-execution-chain/chain.js"></script>
```
Certain version
```html
<script src="https://philippn.com/vendor/js-execution-chain/0.3.0/chain.js"></script>
```
### Via NPM

`@todo`

# ◦ USE

```javascript
    /*var chain = */new JsExecutionChain()
    .delay(1000)
    .next(function () {
        // ...
    })
    .sleep(1000)
    .next(function () {
        // ...
    })
    .wait(function () {
        // ...
    })
    .next(function () {
        // ...
    })
    .run();
```

```javascript
    var date = new Date();
    console.log('Start:', date.getSeconds(), date.getMilliseconds());

    /*var chain = */
    new JsExecutionChain('Time')
        .delay(1000)
        .next(function () {
            var date = new Date();
            console.log('+ 1s', date.getSeconds(), date.getMilliseconds());
        })
        .sleep(1000)
        .next(function () {
            var date = new Date();
            console.log('+ 2s', date.getSeconds(), date.getMilliseconds());
        })
        .next(function () {
            var date = new Date();
            console.log('+ 1s', date.getSeconds(), date.getMilliseconds());
        })
        .run();

    var newVar = false;

    /*var chain = */
    new JsExecutionChain('Wait for variable')
        .wait(function () {
            return newVar === true;
        })
        .run();

    setTimeout(function () {
        console.log('newVar is true');
        newVar = true;
    }, 1000);
```
---

`delay(milliseconds)` - sets each step's timeout globally (for all `next()` steps). Default delay is `1` millisecond

---

`sleep(milliseconds)` - extends general delay

---

### Output in console is similar to:


<img src="README/demo-chain.png"/>