var JsExecutionChain = function (title) {
    this.title = title;
    this.steps = [];
    this.delayMilliseconds = 1;
};


// SETTINGS

JsExecutionChain.prototype.delay = function (delayMilliseconds) {
    this.delayMilliseconds = delayMilliseconds;
    return this;
}

JsExecutionChain.prototype.sleep = function (milliseconds) {
    this.steps.push(['sleep', milliseconds]);
    return this;
}

JsExecutionChain.prototype.next = function (callback) {
    this.steps.push(['step', callback]);
    return this;
}

JsExecutionChain.prototype.wait = function (callback) {
    this.steps.push(['wait', callback]);
    return this;
}


// EXECUTION

JsExecutionChain.prototype.executeStep = function (i) {
    var _this = this, step = this.steps[i], type = step[0];

    if (type === 'step') {
        var callback = step[1];

        console.log(_this.title + "\n%cChain " + (i + 1) + '. STEP (' + _this.delayMilliseconds + 'ms)', 'color: blue; font-weight: bold');

        return new Promise(function (resolve) {
            setTimeout(function () {
                callback();
                resolve();
            }, _this.delayMilliseconds);
        });
    } else if (type === 'wait') {

        console.log(_this.title + "\n%cChain " + (i + 1) + ". WAIT \n" + step[1], 'color: blue; font-weight: bold');

        return new Promise(function (resolve) {
            var interval = setInterval(function () {
                if (step[1]() === true) {
                    clearInterval(interval);
                    resolve();
                }
            }, 100);
        });
    } else if (type === 'sleep') {

        console.log(_this.title + "\n%cChain " + (i + 1) + '. SLEEP ' + step[1] + 'ms (+' + _this.delayMilliseconds + 'ms)', 'color: grey');

        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve();
            }, step[1]);
        });
    }

}

JsExecutionChain.prototype.run = function () {

    var _this = this, i = 0, counted = this.steps.length;

    function recursive(i) {

        if (i === 0) {
            console.log(_this.title + "\n%cJavaScript execution chain START", 'color: red; font-weight: bold');
        }

        var step = _this.steps[i], type = step[0];

        _this.executeStep(i).then(
            function () {
                i++;
                if (i < counted) {
                    recursive(i);
                } else {
                    console.log(_this.title + "\n%cJavaScript execution chain FINISH", 'color: red; font-weight: bold');
                }
            }
        );
    }

    recursive(i)

    return this;
}